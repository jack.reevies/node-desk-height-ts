import 'source-map-support/register';
import fetch from 'node-fetch'
import { config as envConfig } from 'dotenv'
import { discordColour, getDay, getStartOfDay, wait } from './helpers';
import { createHeight, getChangesOnDay, getLastDaily, getLastHeight, recordDaily, startConnection } from './mongo';
import { send } from './discord';
import { formatMsSpanWords, TimeUnit } from "@spacepumpkin/format-timespan"
envConfig()

interface ESPHeight {
  uptime: number
  height: number
}

enum Activity {
  Sit = 0,
  Stand = 1
}

process.on('unhandledRejection', handleError)
process.on('uncaughtException', handleError)

function handleError(err: Error | unknown) {
  if (err instanceof Error) {
    console.error(err.stack)
    return
  }
  console.error(err)
}

async function start() {
  await startConnection()
  endlessGetHeightFromEsp32()
  postOnNewDay()
}

async function endlessGetHeightFromEsp32() {
  if (!process.env.ESP_SERVER) {
    return
  }

  let lastRecord = await getLastHeight()
  let lastHeight = lastRecord?.height || 0
  let lastDay = lastRecord?.day || 0

  while (true) {
    await wait(1000)
    const rawJson = await fetch(process.env.ESP_SERVER)
    try {
      const json = await rawJson.json() as ESPHeight
      if (lastDay === getDay() && isSimilarHeight(lastHeight, json.height)) continue
      await createHeight(json.height)
      lastDay = getDay()
      lastHeight = json.height
    } catch (e) {
      continue
    }
  }
}

async function postOnNewDay() {
  let lastRecordedDay = await getLastDaily()

  while (true) {
    await wait(1000)
    const currentDay = getDay()

    for (let i = lastRecordedDay + 1; i < currentDay; i++) {
      const stats = await calculateDailyStats(i)
      if (!stats) continue
      await sendDiscordMessage(i, stats)
      recordDaily(i, stats.standing, stats.sitting)
    }
    lastRecordedDay = currentDay - 1
  }
}

async function sendDiscordMessage(day: number, stats: { sitting: number, standing: number }) {
  const startOfDay = new Date(getStartOfDay(day))
  const dayOfWeek = startOfDay.getDay()
  const colour = discordColour(dayOfWeek / 7)
  return send(colour, `Sit Stand stats for ${startOfDay.toDateString()}`, [
    {
      name: 'Sat for',
      value: formatMsSpanWords(stats.sitting, TimeUnit.Minute)
    },
    {
      name: 'Stood for',
      value: formatMsSpanWords(stats.standing, TimeUnit.Minute)
    }
  ])
}

async function calculateDailyStats(day: number) {
  const changes = await getChangesOnDay(day)
  const stats = { sitting: 0, standing: 0 }

  if (!changes.length) {
    return null
  }

  let lastState = Activity.Sit
  let lastTime = getStartOfDay(day)

  for (const change of changes) {
    const newState = isSitOrStand(change.height)
    if (newState === lastState) continue

    const timeDiff = change.time - lastTime
    if (newState === Activity.Sit) {
      stats.standing += timeDiff
    } else {
      stats.sitting += timeDiff
    }

    lastState = newState
    lastTime = change.time
  }

  const nextDayStart = getStartOfDay(day + 1)

  const timeDiff = Math.min(Date.now(), nextDayStart) - lastTime
  if (lastState === Activity.Sit) {
    stats.sitting += timeDiff
  } else {
    stats.standing += timeDiff
  }

  return stats
}

function isSitOrStand(height: number, standThreshold: string | number = process.env.STAND_THRESHOLD) {
  if (height >= Number(standThreshold)) {
    return Activity.Stand
  }
  return Activity.Sit
}

function isSimilarHeight(lastHeight: number, newHeight: number, marginOfError: number = 5) {
  return newHeight - marginOfError <= lastHeight &&
    newHeight + marginOfError >= lastHeight
}

start()