import { connect as Mongo, connection, Document, model, Schema } from "mongoose"
import { getDay, getStartOfDay } from "./helpers"

interface Height extends Document {
  time: number
  day: number
  height: number
}

interface Daily extends Document {
  day: number
  time: number
  standing: number
  sitting: number
}

const heightSchema = new Schema<Height>({
  time: { type: Number, unique: true },
  day: { type: Number, index: true },
  height: { type: Number }
})

const dailySchema = new Schema<Daily>({
  day: { type: Number, unique: true },
  time: { type: Number },
  standing: { type: Number },
  sitting: { type: Number }
})

const mHeight = model<Height>('height', heightSchema)
const mDaily = model<Daily>('daily', dailySchema)

export async function createHeight(height: number) {
  return mHeight.create([{ time: Date.now(), day: getDay(), height }])
}

export async function getLastHeight() {
  return mHeight.findOne({}).sort({ 'time': 'desc' }).lean().exec()
}

export async function recordDaily(day: number, standing: number, sitting: number) {
  return mDaily.create([{ day, time: getStartOfDay(day), standing, sitting }])
}

export async function getLastDaily() {
  const last = await mDaily.findOne({}).sort({ 'day': 'desc' }).lean().exec()
  if (!last) return 0
  return last.day
}

export async function getChangesOnDay(day: number) {
  return mHeight.find({ day }).sort({ 'time': 'asc' }).lean().exec()
}

export async function startConnection() {
  try {
    await Mongo(process.env.MONGO_CONN || '')
    connection.on('error', err => {
      // Handle this - I don't know what to look for
      console.error(err)
      process.exit(1)
    })
  }
  catch (e: any) {
    // Do something on error - we should alert the owner maybe?
    console.error(`Unable to connect to Mongo - ${e.message}`)
    process.exit(1)
  }
}