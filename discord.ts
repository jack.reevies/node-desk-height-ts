import fetch from "node-fetch"

export function send(color: string, title: string, fields: { name: string, value: string }[]) {
  return fetch(process.env.DISCORD_WEBHOOK, {
    method: 'POST',
    body: JSON.stringify({
      content: "",
      embeds: [
        {
          color: parseInt(color, 16),
          title,
          fields
        }
      ]
    }),
    headers: {
      "content-type": "application/json"
    }
  })
}