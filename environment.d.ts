declare global {
  namespace NodeJS {
    interface ProcessEnv {
      MONGO_CONN: string
      ESP_SERVER: string
      STAND_THRESHOLD: string
      DISCORD_WEBHOOK: string
    }
  }
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {}