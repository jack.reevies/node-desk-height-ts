const MILLI_IN_DAY = 1000 * 60 * 60 * 24
const START_EPOCH = 1646629200000

export const Colours = [
  '#EF9A9A',
  '#F48FB1',
  '#CE93D8',
  '#B39DDB',
  '#9FA8DA',
  '#90CAF9',
  '#81D4FA',
  '#80DEEA',
  '#80CBC4',
  '#A5D6A7',
  '#C5E1A5',
  '#E6EE9C',
  '#FFF59D',
  '#FFE082',
  '#FFCC80',
  '#FFAB91'
].reverse()

export function getDay(epoch?: number) {
  if (!epoch) {
    epoch = Date.now()
  }
  return Math.ceil((epoch - START_EPOCH) / MILLI_IN_DAY)
}

export function getStartOfDay(day: number) {
  return (day - 1) * MILLI_IN_DAY + START_EPOCH
}

export function wait(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export function discordColour(percentage: number) {
  if (percentage > 1) {
    percentage /= 100
  }

  const targetIndex = Math.round(Colours.length * percentage) - 1

  return Colours[targetIndex]
}